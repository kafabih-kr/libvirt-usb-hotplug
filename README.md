# libvirt-usb-hotplug

This is a script that can be used to attach and detach USB devices from qemu/kvm
virtual machines based on udev rules.

# Usage

First turn on your qemu/kvm virtual machine then find vendor id and product id, 
using lsusb command on terminal. 

After running lsusb, first make sure that the script is executable:
<code>chmod +x libvirt-usb-hotplug.sh</code>

Then run this script base on this arguments:
<code>$ libvrt-usb-hotplug.sh domain action(add/remove) vendor-id product-id</code>

1. Domain is your virtual machine name that you using right now.
2. Action is what you want to do for your USB devices the argument is only 
   available for add or remove.
3. Vendor-id is what your vendor-id on your plugged USB devices. see lsusb 
   first!
4, Product-id is what your product-id on your plugged USB devices. see lsusb
   first!
   
# Troubleshooting

# Running script logs

The script is configured to send all its output to syslog when it is executed 
from udev. Where it is logged depends on the host configuration and OS. On 
Debian / Ubuntu derivative it defaults to showing up in /var/log/syslog.

# Inspecting the current QEMU state

Sometimes it may be useful to check the current QEMU state. This can be done 
by using virsh qemu-monitor-command:

<code>$ virsh qemu-monitor-command testvm-01 --hmp 'info usb'
  Device 0.9, Port 2, Speed 12 Mb/s, Product QEMU USB Hub</code>

Here we can see that the only device attached is a virtual USB hub. Once we have
added a device, it will show up:

<code>$ virsh qemu-monitor-command testvm-01 --hmp 'info usb'
  Device 0.9, Port 2, Speed 12 Mb/s, Product QEMU USB Hub
  Device 0.12, Port 2.3, Speed 12 Mb/s, Product USB to ATA/ATAPI bridge</code>
