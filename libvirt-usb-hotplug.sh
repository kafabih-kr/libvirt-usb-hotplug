#!/bin/bash

#
# Kafabih Rahmat 2019 libvirt-usb-hotplug.sh
# This Script is fork from
# usb-libvirt-hotplug.sh
#
# This script can be used to hotplug USB devices to libvirt virtual
# machines from udev rules.
#
# This can be used to attach devices when they are plugged into a
# specific port on the host machine.
#
# See: https://github.com/olavmrk/usb-libvirt-hotplug
#

# Abort script execution on errors
set -e

PROG="$(basename "$0")"

if [ ! -t 1 ]; then
  # stdout is not a tty. Send all output to syslog.
  coproc logger --tag "${PROG}"
  exec >&${COPROC[1]} 2>&1
fi

DOMAIN="$1"
if [ -z "${DOMAIN}" ]; then
  echo "Usage: 
$ ${PROG} libvirt-domain/qemu-domain action(add/remove) vendor-id product-id
To find vendor id and product id, don't forget to run lsusb first!" >&2
  exit 1
fi

ACTION="$2"
if [ -z "${ACTION}" ]; then
  echo "Missing udev ACTION environment variable." >&2
  exit 1
fi
if [ "${ACTION}" == 'add' ]; then
  COMMAND='attach-device'
elif [ "${ACTION}" == 'remove' ]; then
  COMMAND='detach-device'
else
  echo "Invalid udev ACTION: ${ACTION}" >&2
  exit 1
fi

VENDORID="$3"
if [ -z "${VENDOR-ID}" ]; then
  echo "Missing udev VENDOR-ID environment variable." >&2
  exit 1
fi
PRODUCTID="$4"
if [ -z "${PRODUCTID}" ]; then
  echo "Missing udev PRODUCT-ID environment variable." >&2
  exit 1
fi

#
# Now we translate the vendor-id and product-id to libvirt
# xml configuration using the 0x in front of  vendor-id 
# and product-id.
#

VENDORID=$((0x$VENDORID))
PRODUCTID=$((0x$PRODUCTID))

#
# Now we have all the information we need to update the VM.
# Run the appropriate virsh-command, and ask it to read the
# update XML from stdin.add
#
echo "Executing virsh ${COMMAND} ${DOMAIN} for USB vendor id=${VENDORID} product id=${PRODUCTID}:" >&2
virsh "${COMMAND}" "${DOMAIN}" /dev/stdin <<END
<hostdev mode='subsystem' type='usb'>
  <source>
    <vendor id='${VENDORID}'/> 
    <product id='${PRODUCTID}'/>
  </source>
</hostdev>
END
